package com.userservice.UserService.dto;

import javax.validation.constraints.NotBlank;

public class OrderDTO {

    private Long id;

    @NotBlank(message = "userId is mandatory")
    private Long userId;

    @NotBlank(message = "orderId is mandatory")
    private String orderId;

    public OrderDTO() {
    }

    public OrderDTO(Long id, Long userId, String orderId) {
        this.id = id;
        this.userId = userId;
        this.orderId = orderId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
