package com.userservice.UserService.dto;

import javax.validation.constraints.NotBlank;

public class UserDTO {

    private Long id;
    @NotBlank(message = "Name is mandatory")
    private String name;
    @NotBlank(message = "Age is mandatory")
    private String age;

    public UserDTO() {}

    public UserDTO(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public UserDTO(Long id, String name, String age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}